from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Chrome(executable_path='path_to_chromedriver')

# Navigate to Google's homepage
driver.get("https://www.google.com")

# Find the search bar element and type your query
search_box = driver.find_element_by_name("q")
search_query = "Your search query goes here"
search_box.send_keys(search_query)

# Press 'Enter' to perform the search
search_box.send_keys(Keys.RETURN)

# Wait for the search results to load
time.sleep(3)  # You can adjust the wait time based on your internet speed
